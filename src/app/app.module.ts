import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouterModule, Routes} from "@angular/router";

import {CustomMaterialModule} from "./core/material.module";


import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { WoformComponent } from './forms/woform/woform.component';
import { HeaderComponent } from './header/header.component';
import { MatDialogModule, MatMenuModule, MatDatepicker, MatDatepickerModule, MatButtonToggleModule, MatButtonToggleGroup, MatButtonToggleGroupMultiple, MatInputModule, MatSelectModule } from '@angular/material';
import { WorkOrderComponent } from './forms/work-order/work-order.component';
import { FirstComponent } from './pages/first/first.component';
import { SecondComponent } from './pages/second/second.component';
import { ThirdComponent } from './pages/third/third.component';
import { FourthComponent } from './pages/fourth/fourth.component';
import { TestComponent } from './ui-elements/test/test.component';
import { WorkOrderRequestComponent } from './forms/work-order-request/work-order-request.component';
import { FormsComponent } from './pages/forms/forms.component';
import { RequestsComponent } from './forms/requests/requests.component';
import { LocationComponent } from './forms/location/location.component';
import { AssetComponent } from './forms/asset/asset.component';
import { PartsInventoryComponent } from './forms/parts-inventory/parts-inventory.component';
import { PurchaseOrderComponent } from './forms/purchase-order/purchase-order.component';
import { MeterComponent } from './forms/meter/meter.component';
import { InviteUserComponent } from './forms/invite-user/invite-user.component';
import { VendorComponent } from './forms/vendor/vendor.component';
import { CategoriesComponent } from './forms/categories/categories.component';



const appRoutes: Routes = [
  { path: '', component: FirstComponent, data: { title: 'First Component' } },
  { path: 'first', component: FirstComponent, data: { title: 'First Component' } },
  { path: 'second', component: SecondComponent, data: { title: 'Second Component'} },
  { path: 'third', component: ThirdComponent, data: { title: 'third Component'} },
  { path: 'fourth', component: FourthComponent, data: { title: 'fourth Component'} },
  { path: 'forms', component: FormsComponent, data: { title: 'Forms Component'} },
  { path: 'createwo', component: WoformComponent, data: { title: 'Create Work order'} },
  { path: 'workorderreq', component: WorkOrderRequestComponent, data: { title: 'Work order request'} },
  { path: 'requests', component: RequestsComponent, data: { title: 'Requests'} },
  { path: 'location', component: LocationComponent, data: { title: 'Locations'} },
  { path: 'asset', component: AssetComponent, data: { title: 'Assets'} },
  { path: 'partsinventory', component: PartsInventoryComponent, data: { title: 'Parts Inventory'} },
  { path: 'purchaseorder', component: PurchaseOrderComponent, data: { title: 'Purchase Order'} },
  { path: 'meter', component: MeterComponent, data: { title: 'Meter'} },
  { path: 'inviteuser', component: InviteUserComponent, data: { title: 'Invite User'} },
  { path: 'vendor', component: VendorComponent, data: { title: 'Vendor'} },
  { path: 'categories', component: CategoriesComponent, data: { title: 'Categories'} }

];
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SecondComponent,
    FirstComponent,
    WoformComponent,
    ThirdComponent,
    HeaderComponent,
    FourthComponent,
    WorkOrderComponent,
    TestComponent,
    WorkOrderRequestComponent,
    FormsComponent,
    RequestsComponent,
    LocationComponent,
    AssetComponent,
    PartsInventoryComponent,
    PurchaseOrderComponent,
    MeterComponent,
    InviteUserComponent,
    VendorComponent,
    CategoriesComponent
  ],
  entryComponents: [WoformComponent],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true } // <-- debugging purposes only
    ),
    CustomMaterialModule,
    MatDialogModule,
    MatDatepickerModule,
    MatButtonToggleModule,
    MatInputModule,
    MatSelectModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
