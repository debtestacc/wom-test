import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TestComponent } from 'src/app/ui-elements/test/test.component';
import { WoformComponent } from 'src/app/forms/woform/woform.component';



@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {
  title='pop up modal for form';
  constructor(public dialog: MatDialog) {}
  openDialog(){
    this.dialog.open(TestComponent)
  }

  ngOnInit() {
  }
}
