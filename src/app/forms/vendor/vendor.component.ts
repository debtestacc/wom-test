import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {
  vendorForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.vendorForm = this.fb.group({
      companyName:[''],
      address:[''],
      phone:[''],
      website:[''],
      name:[''],
      email:[''],
      vendorType:[''],
      desc:['']
    });

  }
  onSubmit() {
    console.log(this.vendorForm);
    console.log('Saved: ' + JSON.stringify(this.vendorForm.value));
  }
}
