import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {
  requestForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.requestForm =this.fb.group({
      title:[''],
      desc:[''],
      priority:[''],
      image:[''],
      files: this.fb.array([])
    })
  }
  get addDynamicElement() {
    return this.requestForm.get('files') as FormArray
  }

  addFile() {
    this.addDynamicElement.push(this.fb.control(''))
  }

  onSubmit() {
    console.log(this.requestForm);
    console.log('Saved: ' + JSON.stringify(this.requestForm.value));
  }
}
