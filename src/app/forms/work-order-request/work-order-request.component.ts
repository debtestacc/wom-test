import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-work-order-request',
  templateUrl: './work-order-request.component.html',
  styleUrls: ['./work-order-request.component.css']
})
export class WorkOrderRequestComponent implements OnInit {
  workOrderRequestForm: FormGroup;
  
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.workOrderRequestForm = this.fb.group({
      requesterInfoGroup: this.fb.group({
        fullName: [''],
        phone: [''],
        email: [''],
      }),
      requestDetailsGroup: this.fb.group({
        title: [''],
        desc: [''],
        dueDate: ['']
      }),
      filesGroup: this.fb.group({
        image: [''],
        file: ['']
      })
    });

  }
  onSubmit() {
    console.log(this.workOrderRequestForm);
    console.log('Saved: ' + JSON.stringify(this.workOrderRequestForm.value));
  }


}
