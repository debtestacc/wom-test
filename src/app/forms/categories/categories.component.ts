import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categoriesForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.categoriesForm = this.fb.group({
      categoryName:['']
    });
  }
  onSubmit() {
    console.log(this.categoriesForm);
    console.log('Saved: ' + JSON.stringify(this.categoriesForm.value));
  }

}
