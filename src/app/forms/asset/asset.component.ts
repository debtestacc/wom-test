import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.css']
})
export class AssetComponent implements OnInit {
  assetForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.assetForm = this.fb.group({
      imageDrop:[''],
      title:[''],
      desc:[''],
      trackCheckinCheckout:[''],
      model:[''],
      barcode:[''],
      category:[''],
      location:[''],
      area:[''],
      parentAsset:[''],
      worker:[''],
      additionalWorker:[''],
      team:[''],
      vendors:[''],
      customers:[''],
      purchaseDate:[''],
      placedInService:[''],
      purchasePrice:[''],
      warrantyExpiration:[''],
      residualPrice:[''],
      usefulLife:[''],
      additionalInformation:[''],
      addFile:['']
    })

  }
  onSubmit() {
    console.log(this.assetForm);
    console.log('Saved: ' + JSON.stringify(this.assetForm.value));
  }

}
