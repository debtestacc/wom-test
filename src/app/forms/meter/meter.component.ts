import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-meter',
  templateUrl: './meter.component.html',
  styleUrls: ['./meter.component.css']
})
export class MeterComponent implements OnInit {
  meterForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.meterForm = this.fb.group({
      title:[''],
      uom:[''],
      upgradeFrequency:[''],
      category:[''],
      image:[''],
      workers:[''],
      asset:['']
    });
  }
  onSubmit() {
    console.log(this.meterForm);
    console.log('Saved: ' + JSON.stringify(this.meterForm.value));
  }
}
