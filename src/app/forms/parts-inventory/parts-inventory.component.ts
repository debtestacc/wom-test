import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-parts-inventory',
  templateUrl: './parts-inventory.component.html',
  styleUrls: ['./parts-inventory.component.css']
})
export class PartsInventoryComponent implements OnInit {
  partsInventoryForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.partsInventoryForm = this.fb.group({
      title:[''],
      desc:[''],
      category:[''],
      cost:[''],
      quantity:[''],
      minQuantity:[''],
      nonStock:[''],
      barcode:[''],
      area:[''],
      addPartDetails:[''],
      workers:[''],
      teams:[''],
      vendors:[''],
      customer:[''],
      // customData:[''],
      location:[''],
      addAFile:[''],
      // addFile:[''],
      addImage:['']
    })
  }
  onSubmit() {
    console.log(this.partsInventoryForm);
    console.log('Saved: ' + JSON.stringify(this.partsInventoryForm.value));
  }

}
