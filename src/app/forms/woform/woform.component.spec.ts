import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoformComponent } from './woform.component';

describe('WoformComponent', () => {
  let component: WoformComponent;
  let fixture: ComponentFixture<WoformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WoformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
